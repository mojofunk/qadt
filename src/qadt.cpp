#include "qadt.h"
#include "qadt-private.h"

#include "qadt/private/sourceincludes.h"

#include "qadt/application.cpp"
#include "qadt/developerwindow.cpp"
#include "qadt/libraryoptions.cpp"
