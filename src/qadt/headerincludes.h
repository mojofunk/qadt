#ifndef QADT_HEADERINCLUDES_H
#define QADT_HEADERINCLUDES_H

/**
 * @file
 *
 * These are all the includes required by all header files in the qadt
 * module to compile successfully.
 */

#include <adt.hpp>

#include <vector>
#include <cctype>

#include <QtCore/QCommandLineParser>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QSortFilterProxyModel>
#include <QtCore/QTimer>

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QTableWidget>

#include "qadt/visibility.h"

#endif // QADT_HEADERINCLUDES_H
