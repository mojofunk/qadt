#ifndef QADT_VISIBILITY_H
#define QADT_VISIBILITY_H

#ifdef QADT_STATIC

#define QADT_API
#define QADT_CAPI
#define QADT_LOCAL

#else // shared library is default

#ifdef _MSC_VER
#define QADT_EXPORT __declspec(dllexport)
#define QADT_IMPORT __declspec(dllimport)
#define QADT_LOCAL
#else
#define QADT_EXPORT __attribute__((visibility("default")))
#define QADT_IMPORT __attribute__((visibility("default")))
#define QADT_LOCAL __attribute__((visibility("hidden")))
#endif

#ifdef QADT_BUILDING_DLL
#define QADT_API QADT_EXPORT
#else
#define QADT_API QADT_IMPORT
#endif

#endif

#endif // QADT_VISIBILITY_H
