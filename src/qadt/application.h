#ifndef QADT_APPLICATION_H
#define QADT_APPLICATION_H

#include "qadt/headerincludes.h"

namespace qadt
{

class DeveloperWindow;

class QADT_API Application
{
public:
    /**
     * Use this constructor if using external option parsing and setting
     * LibraryOptions manually before creating Application instance.
     */
    Application();

    /**
     * Let Application parse the command line and remove relevant options
     */
    Application(int* argc, char*** argv);

    ~Application();

public:
    static DeveloperWindow* developerWindow();

public:
    static void setSourceSearchPaths(std::vector<std::string> const& paths);

    static std::vector<std::string> sourceSearchPaths();
};

} // namespace qadt

#endif // QADT_APPLICATION_H
