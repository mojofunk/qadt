#ifndef QADT_DEVELOPERWINDOW_H
#define QADT_DEVELOPERWINDOW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class DeveloperWindowPrivate;

class QADT_API DeveloperWindow : public QMainWindow
{
    Q_OBJECT

public:
    DeveloperWindow();

    ~DeveloperWindow();

private Q_SLOTS:
    // void on_selected_log_record_changed();

private:
    DeveloperWindowPrivate* d;
};

} // namespace qadt

#endif // QADT_DEVELOPERWINDOW_H
