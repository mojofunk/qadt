#include "qadt-private.h"

namespace qadt
{

struct LibraryOptionsPrivate {
    QString m_logCategories;
    QString m_recordTypes;
    QString m_logFile;

    bool m_enableTrace = false;

    bool m_optionsParsed = false;
};

LibraryOptions&
LibraryOptions::instance()
{
    static LibraryOptions options;
    return options;
}

LibraryOptions::LibraryOptions()
    : d(new LibraryOptionsPrivate)
{
#if 0
  Glib::OptionEntry entry;

  entry.set_long_name("log-categories");
  entry.set_short_name('L');
  entry.set_description(_(
      "Set enabled log categories. Use \"-L list\" to see available options."));
  add_entry(entry, d->log_categories);

  entry.set_long_name("log-file");
  entry.set_short_name('l');
  entry.set_description(_("Set log output file."));
  add_entry_filename(entry, d->log_file);

  entry.set_long_name("log-enable-trace");
  entry.set_short_name('t');
  entry.set_description(_("Set stack tracing enabled by default for logging."));
  add_entry(entry, d->enable_trace);

  entry.set_long_name("log-record-types");
  entry.set_short_name('R');
  entry.set_description(
      _("Set enabled log record types (all, message, call, data, object)."));
  add_entry(entry, d->record_types);
#endif
}

LibraryOptions::~LibraryOptions()
{

}

void
LibraryOptions::setLogCategoriesString(const QString& str)
{
    instance().d->m_logCategories = str;
}

QString
LibraryOptions::logCategoriesString()
{
    return instance().d->m_logCategories;
}

void
LibraryOptions::setRecordTypesString(const QString& str)
{
    instance().d->m_recordTypes = str;
}

QString
LibraryOptions::recordTypesString()
{
    return instance().d->m_recordTypes;
}

void
LibraryOptions::setLogFilePath(const QString& file_path)
{
    instance().d->m_logFile = file_path;
}

QString
LibraryOptions::logFilePath()
{
    return instance().d->m_logFile;
}

void
LibraryOptions::setTraceEnabled(bool enable)
{
    instance().d->m_enableTrace = enable;
}

bool
LibraryOptions::traceEnabled()
{
    return instance().d->m_enableTrace;
}

void
LibraryOptions::addCommandLineOptions(QCommandLineParser& parser)
{
}

bool
LibraryOptions::parseCommandLine()
{
#if 0
 parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
    const QCommandLineOption nameServerOption("n", "The name server to use.", "nameserver");
    parser.addOption(nameServerOption);
    const QCommandLineOption typeOption("t", "The lookup type.", "type");
    parser.addOption(typeOption);
    parser.addPositionalArgument("name", "The name to look up.");
    const QCommandLineOption helpOption = parser.addHelpOption();
    const QCommandLineOption versionOption = parser.addVersionOption();

    if (!parser.parse(QCoreApplication::arguments())) {
        *errorMessage = parser.errorText();
        return CommandLineError;
    }

    if (parser.isSet(versionOption))
        return CommandLineVersionRequested;

    if (parser.isSet(helpOption))
        return CommandLineHelpRequested;

    if (parser.isSet(nameServerOption)) {
        const QString nameserver = parser.value(nameServerOption);
        query->nameServer = QHostAddress(nameserver);
        if (query->nameServer.isNull() || query->nameServer.protocol() == QAbstractSocket::UnknownNetworkLayerProtocol) {
            *errorMessage = "Bad nameserver address: " + nameserver;
            return CommandLineError;
        }
    }

    if (parser.isSet(typeOption)) {
        const QString typeParameter = parser.value(typeOption);
        const int type = typeFromParameter(typeParameter.toLower());
        if (type < 0) {
            *errorMessage = "Bad record type: " + typeParameter;
            return CommandLineError;
        }
        query->type = static_cast<QDnsLookup::Type>(type);
    }

    const QStringList positionalArguments = parser.positionalArguments();
    if (positionalArguments.isEmpty()) {
        *errorMessage = "Argument 'name' missing.";
        return CommandLineError;
    }
    if (positionalArguments.size() > 1) {
        *errorMessage = "Several 'name' arguments specified.";
        return CommandLineError;
    }
    query->name = positionalArguments.first();

    return CommandLineOk;
#endif
	return true;
}

void
LibraryOptions::enableTrace()
{
    adt::Log::set_trace_enabled(traceEnabled());
}

bool
LibraryOptions::enableCategories()
{

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    auto const categoryStrings
        = logCategoriesString().split(',', Qt::SkipEmptyParts);
#else
    auto const categoryStrings
        = logCategoriesString().split(',', QString::SkipEmptyParts);
#endif

    std::vector<std::string> categoriesToEnable;

    for (auto const& category : categoryStrings) {
        if (category == "list") {
            for (auto const& c : adt::Log::get_categories()) {
                std::cout << c->name << std::endl;
            }
            exit(0);
        }

        if (category == "all") {
            adt::Log::set_all_categories_enabled(true);
        }

        categoriesToEnable.emplace_back(std::move(category.toStdString()));
    }

    adt::Log::set_categories_enabled(categoriesToEnable, true);

    return categoriesToEnable.size();
}

void
LibraryOptions::enableRecordTypes()
{
#if 0
	auto const type_strings = getRecordTypesString().split(',');

	for (auto const& type_string : type_strings) {
		if (type_string == "all") {
			adt::Log::set_all_types_enabled(true);
			return;
		}

		if (type_string == "message") {
			adt::Log::set_message_type_enabled(true);
			continue;
		}

		if (type_string == "allocation") {
			adt::Log::set_allocation_type_enabled(true);
			continue;
		}

		if (type_string == "call") {
			adt::Log::set_function_type_enabled(true);
			continue;
		}

		if (type_string == "data") {
			adt::Log::set_data_type_enabled(true);
			continue;
		}
	}
#endif
}

} // namespace qadt
