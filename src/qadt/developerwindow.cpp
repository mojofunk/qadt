#include "qadt-private.h"

namespace qadt
{

DeveloperWindow::DeveloperWindow()
    : d(new DeveloperWindowPrivate())
{
    setCentralWidget(d);

    setWindowTitle("Developer Window");
}

DeveloperWindow::~DeveloperWindow()
{
}

} // namespace qadt
