#ifndef QADT_LIBRARYOPTIONS_H
#define QADT_LIBRARYOPTIONS_H

#include "qadt/headerincludes.h"

namespace qadt
{

struct LibraryOptionsPrivate;

class QADT_API LibraryOptions
{
public:
    /**
     * The instance is required if an application wants to add the
     * OptionGroup to the global option context
     */
    static LibraryOptions& instance();

public:
    /**
     * Add qadt specific command line options to QCommandLineParser
     *
     * If an application needs to have its own command line options (which
     * it probably will), then addCommandLineOptions can be used to add
     * the default qadt options. It will then have to parse the command
     * line and set the library options below.
     */
    static void addCommandLineOptions(QCommandLineParser& parser);

    /**
     * parse the command line.
     *
     * @return true if successfully parsed.
     */
    static bool parseCommandLine();

public:
    /* The set methods are only relevant if parse_options isn't called to
     * set LibraryOption state, for instance if a different command line
     * API was being used.
     */
    static void setLogCategoriesString(const QString& categories);

    static QString logCategoriesString();

    static void setRecordTypesString(const QString& record_types);

    static QString recordTypesString();

    static void setLogFilePath(const QString& file_path);

    static QString logFilePath();

    static void setTraceEnabled(bool enable = true);

    static bool traceEnabled();

    // This API seems like it is in the wrong place...
public: // Set adt::Log state based on option state
    static void enableTrace();

    // @return true If categories where enabled
    static bool enableCategories();

    static void enableRecordTypes();

private:
    std::unique_ptr<LibraryOptionsPrivate> d;

private: // Ctors
    LibraryOptions();
    ~LibraryOptions();
};

} // namespace qadt

#endif // QADT_LIBRARYOPTIONS_H
