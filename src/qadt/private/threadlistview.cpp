#include "qadt-private.h"

namespace qadt
{

ThreadListView::ThreadListView()
    : m_table(new ThreadTableWidget(this))
{
    auto vlayout = new QVBoxLayout;

    vlayout->addWidget(m_table);

    setLayout(vlayout);
}

} // namespace qadt
