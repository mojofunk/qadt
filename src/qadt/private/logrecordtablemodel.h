#ifndef QADT_LOGRECORDTABLEMODEL_H
#define QADT_LOGRECORDTABLEMODEL_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogRecordTableModel : public QAbstractTableModel
{
    Q_OBJECT

public: // QAbstractTableModel
    LogRecordTableModel(QObject* parent);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex& index,
                  int role = Qt::DisplayRole) const override;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const override;

public:
    std::shared_ptr<adt::Record> recordAtIndex(const QModelIndex& index) const;

public Q_SLOTS:
    void refresh();

private:
    QString messageStringFromRecord(const adt::Record& record) const;

    QString threadIdToName(const adt::Record& record) const;

private:
    enum Columns {
        CATEGORY,
        RECORD_TYPE,
        MESSAGE,
        THREAD_ID,
        DURATION,
        FUNCTION_NAME,
        LINE,
        FILE_PATH,
        COUNT
    };

private:
    std::vector<std::shared_ptr<adt::Record>> m_records;
};

} // namespace qadt

#endif // QADT_LOGRECORDTABLEMODEL_H
