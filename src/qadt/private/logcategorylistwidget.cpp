#include "qadt-private.h"

namespace qadt
{

LogCategoryListWidget::LogCategoryListWidget(QWidget* parent)
    : QListWidget(parent)
{
    setSelectionMode(QAbstractItemView::NoSelection);

    refresh();

    connect(this, &QListWidget::itemChanged, this,
            &LogCategoryListWidget::onItemChanged);
}

void
LogCategoryListWidget::refresh()
{
    clear();

    auto const categories = adt::Log::get_categories();

    auto name_cmp = [](const adt::LogCategory* a, const adt::LogCategory* b) {
        std::string a_name(a->name);
        std::string b_name(b->name);

        auto const to_lower
            = [](const unsigned char i) { return std::tolower(i); };

        std::transform(a_name.begin(), a_name.end(), a_name.begin(), to_lower);
        std::transform(b_name.begin(), b_name.end(), b_name.begin(), to_lower);

        return (a_name.compare(b_name) < 0);
    };

    std::set<adt::LogCategory*, decltype(name_cmp)> name_sorted_categories(
        categories.begin(), categories.end(), name_cmp);

    for (auto const& category : name_sorted_categories) {
        auto item = new QListWidgetItem(QString(category->name));
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(category->enabled ? Qt::Checked : Qt::Unchecked);
        QVariant variant;
        variant.setValue(category);
        item->setData(Qt::UserRole, variant);
        addItem(item);
    }
}

void
LogCategoryListWidget::toggleVisibleItemsEnabled()
{
    bool firstRow = true;
    bool enable = false;

    for (int row = 0; row < count(); ++row)
    {
        if (!item(row)->isHidden()) {
            if (firstRow) {
                firstRow = false;
                enable = !item(row)->checkState();
                // ? true : false;
            }
            item(row)->setCheckState(enable ? Qt::Checked : Qt::Unchecked);

            QVariant variant = item(row)->data(Qt::UserRole);
            auto category = qvariant_cast<adt::LogCategory*>(variant);

            category->enabled = enable;
        }
    }
}

void
LogCategoryListWidget::onItemChanged(QListWidgetItem* item)
{
    QVariant variant = item->data(Qt::UserRole);
    auto category = qvariant_cast<adt::LogCategory*>(variant);

    auto const enabled = item->checkState() ? true : false;

    assert(category->enabled != enabled);

    category->enabled = enabled;
}

void
LogCategoryListWidget::setCategoryFilter(const QString& str)
{
  for (int row = 0; row < count(); ++row) {
      if (item(row)->text().contains(str, Qt::CaseInsensitive)) {
          item(row)->setHidden(false);
      } else {
          item(row)->setHidden(true);
      }
  }
}

} // namespace qadt
