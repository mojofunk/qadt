#ifndef QADT_TRACEVIEW_H
#define QADT_TRACEVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class TraceTableWidget;

class TraceView : public QWidget
{
    Q_OBJECT

public:
    TraceView();

public:
    // Access to signalSelectedRecordChanged()
    TraceTableWidget* traceTableWidget() const { return m_traceTableWidget; }

private:
    TraceTableWidget* m_traceTableWidget;
};

} // namespace qadt

#endif // QADT_TRACEVIEW_H
