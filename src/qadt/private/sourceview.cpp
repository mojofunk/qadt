#include "qadt-private.h"

namespace qadt
{

SourceView::SourceView()
{
    auto vlayout = new QVBoxLayout;

    m_textEdit = new QPlainTextEdit(this);
    m_textEdit->setReadOnly(true);

    vlayout->addWidget(m_textEdit);

    setLayout(vlayout);
}

void
SourceView::setRecord(std::shared_ptr<adt::Record> record)
{
    if (!record) {
        m_textEdit->setPlainText("");
        return;
    }

    QString filePath(record->src_location.file_path);

    set_file(filePath);
    set_line_number(record->src_location.line);
}

void
SourceView::setStackFrame(std::shared_ptr<adt::StackFrame> stack_frame)
{
    if (!stack_frame || !stack_frame->has_file_name()) {
        m_textEdit->setPlainText("");
        return;
    }

    QString qstr = QString::fromStdString(stack_frame->get_file_name());

    set_file(qstr);

    if (stack_frame->has_line_number()) {
        set_line_number(stack_frame->get_line_number());
    }
}

void
SourceView::set_file(QString const& filePath)
{
	QFile file(filePath);

	if (!file.open(QFile::ReadOnly | QFile::Text)) {
		m_textEdit->setPlainText(QString("Unable to open file : ") + filePath);
		return;
	}

	QTextStream in(&file);
	m_textEdit->setPlainText(in.readAll());
}

void
SourceView::set_line_number(int const lineNumber)
{
	// TODO what happens if lineNumber = 0?
    QTextCursor textCursor(
        m_textEdit->document()->findBlockByLineNumber(lineNumber - 1));
    textCursor.select(QTextCursor::LineUnderCursor);
    m_textEdit->setTextCursor(textCursor);

    m_textEdit->centerCursor();
}

} // namespace qadt
