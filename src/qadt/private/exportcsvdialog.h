#ifndef QADT_EXPORTCSVDIALOG_H
#define QADT_EXPORTCSVDIALOG_H

#include "qadt/headerincludes.h"

namespace qadt
{

class ExportCSVOptions;

class ExportCSVDialog : public QDialog
{
    Q_OBJECT

public: // Ctors
    ExportCSVDialog(const std::shared_ptr<adt::Record>& record,
                    bool using_selected_record);

public: // Methods
    adt::CSVOptions const& options() const;

private: // Data
    std::shared_ptr<adt::Record> m_record;

    std::unique_ptr<adt::CSVOptions> m_options;

    QCheckBox* m_addTimestamp { nullptr };
    QCheckBox* m_addDuration { nullptr };

    std::vector<QCheckBox*> m_propertyCheckboxes;
};

} // namespace qadt

#endif // QADT_EXPORTCSVDIALOG_H
