#include "qadt-private.h"

namespace qadt
{

PropertyListView::PropertyListView()
{
    auto vlayout = new QVBoxLayout;

    m_tableWidget = new QTableWidget(this);
    m_tableWidget->setColumnCount(Columns::COUNT);

    QStringList columns;
    columns << "Name"
            << "Value"
            << "Type"
            << "Function";
    m_tableWidget->setHorizontalHeaderLabels(columns);
    m_tableWidget->verticalHeader()->hide();
    m_tableWidget->setShowGrid(false);
    m_tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    m_tableWidget->horizontalHeader()->setSectionResizeMode(
        QHeaderView::ResizeToContents);
    m_tableWidget->horizontalHeader()->setStretchLastSection(true);

    vlayout->addWidget(m_tableWidget);

    setLayout(vlayout);
}

void
PropertyListView::setRecord(std::shared_ptr<adt::Record> record)
{
    m_tableWidget->setRowCount(0);

    if (!record) {
        return;
    }

    m_record = record;

    for (auto const& prop : record->properties) {
        auto newRow = m_tableWidget->rowCount();
        m_tableWidget->insertRow(newRow);

        auto const sProp = QString::fromStdString(prop.name);
        auto const sValue = QString::fromStdString(prop.value.to_string());
        auto const sType
            = QString::fromStdString(adt::variant_type_name(prop.value.type));

        m_tableWidget->setItem(newRow, Columns::NAME,
                               new QTableWidgetItem(sProp));
        m_tableWidget->setItem(newRow, Columns::VALUE,
                               new QTableWidgetItem(sValue));
        m_tableWidget->setItem(newRow, Columns::TYPE,
                               new QTableWidgetItem(sType));
    }
}

} // namespace qadt
