#ifndef QADT_CLASSLISTVIEW_H
#define QADT_CLASSLISTVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class ClassTableWidget;

class ClassListView : public QWidget
{
    Q_OBJECT

public:
    ClassListView();

private:
    ClassTableWidget* m_table;
};

} // namespace qadt

#endif // QADT_CLASSLISTVIEW_H
