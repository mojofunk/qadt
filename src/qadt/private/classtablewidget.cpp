#include "qadt-private.h"

namespace qadt
{

ClassTableWidget::ClassTableWidget(QWidget* parent)
    : QTableWidget(parent)
{
    setColumnCount(Columns::COUNT);
    QStringList columns;
    columns << "Class Name"
            << "Class Size"
            << "Class Instance Count"
            << "Class Copied Count";
    setHorizontalHeaderLabels(columns);
    setSortingEnabled(true);

    setEditTriggers(QAbstractItemView::NoEditTriggers);

    verticalHeader()->hide();
    setShowGrid(false);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    connect(&ApplicationPrivate::instance()->autoUpdateTimer(),
            &QTimer::timeout, this, &ClassTableWidget::refresh);
}

void
ClassTableWidget::refresh()
{
    auto const classTrackers = adt::ClassTrackerManager::get_class_trackers();

    std::set<int> rowIndexes;

    for (int row = 0; row < rowCount(); ++row) {
        rowIndexes.insert(row);
    }

    setSortingEnabled(false);

    // Add all threads not already represented
    for (auto const& classTracker : classTrackers) {
        bool found = false;

        auto const sName
            = QString::fromStdString(classTracker->get_tracked_class_name());

        uint64_t const sSize = classTracker->get_tracked_class_size();
        auto const sCount = classTracker->get_tracked_class_count();
        auto const sCopiedCount
            = classTracker->get_tracked_class_copied_count();

        for (auto it = rowIndexes.begin(); it != rowIndexes.end(); ++it) {

            QVariant variant
                = item(*it, Columns::CLASS_NAME)->data(Qt::UserRole);
            auto tracker = qvariant_cast<adt::ClassTracker*>(variant);

            if (tracker == classTracker) {
                found = true;
                item(*it, Columns::CLASS_NAME)->setText(sName);
                item(*it, Columns::CLASS_SIZE)->setData(Qt::EditRole, sSize);
                item(*it, Columns::CLASS_COUNT)->setData(Qt::EditRole, sCount);
                item(*it, Columns::CLASS_COPIED_COUNT)
                    ->setData(Qt::EditRole, sCopiedCount);
                rowIndexes.erase(it);
                break;
            }
        }

        if (!found) {
            auto const newRow = rowCount();
            insertRow(newRow);
            auto item = new QTableWidgetItem(sName);
            QVariant variant;
            variant.setValue(classTracker);
            item->setData(Qt::UserRole, variant);
            setItem(newRow, Columns::CLASS_NAME, item);
            item = new QTableWidgetItem;
            item->setData(Qt::EditRole, sSize);
            setItem(newRow, Columns::CLASS_SIZE, item);
            item = new QTableWidgetItem;
            item->setData(Qt::EditRole, sCount);
            setItem(newRow, Columns::CLASS_COUNT, item);
            item = new QTableWidgetItem;
            item->setData(Qt::EditRole, sCopiedCount);
            setItem(newRow, Columns::CLASS_COPIED_COUNT, item);
        }
    }

    // Class Trackers are static and never removed.

    setSortingEnabled(true);
}

} // namespace qadt
