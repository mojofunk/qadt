#include "qadt-private.h"

namespace qadt
{

void
SearchLineEdit::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Escape) {
        setText("");
        return;
    }
    QLineEdit::keyPressEvent(event);
}

} // namespace qadt
