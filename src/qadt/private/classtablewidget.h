#ifndef QADT_CLASSTABLEWIDGET_H
#define QADT_CLASSTABLEWIDGET_H

#include "qadt/headerincludes.h"

namespace qadt
{

class ClassTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    ClassTableWidget(QWidget* parent);

public Q_SLOTS:
    void refresh();

private:
    enum Columns {
        CLASS_NAME,
        CLASS_SIZE,
        CLASS_COUNT,
        CLASS_COPIED_COUNT,
        COUNT
    };
};

} // namespace qadt

#endif // QADT_CLASSTABLEWIDGET_H
