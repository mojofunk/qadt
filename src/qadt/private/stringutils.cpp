#include "qadt-private.h"

namespace qadt
{

QString
threadIdToName(const std::thread::id& thread_id)
{
    auto thread_info = adt::Threads::get_thread_info(thread_id);

    std::ostringstream oss;

    if (thread_info) {
        oss << thread_info->name << "(" << thread_info->id << ")";
    } else {
        // not a registered thread
        oss << thread_id;
    }
    return QString::fromStdString(oss.str());
}

QString
threadIdToString(const std::thread::id& thread_id)
{
    return QString::fromStdString(adt::Threads::to_string(thread_id));
}

ThreadPriorityMap
threadPriorityMap()
{
    return ThreadPriorityMap{ { "None", adt::ThreadPriority::NONE },
                              { "Low", adt::ThreadPriority::LOW },
                              { "Normal", adt::ThreadPriority::NORMAL },
                              { "High", adt::ThreadPriority::HIGH },
                              { "Realtime", adt::ThreadPriority::REALTIME } };
}

QString
threadPriorityToString(const adt::ThreadPriority& priority)
{
    for (auto const& p : threadPriorityMap()) {
        if (p.second == priority) {
            return p.first;
        }
    }
    return QString();
}

adt::ThreadPriority
stringToThreadPriority(const QString& str)
{
    for (auto const& p : threadPriorityMap()) {
        if (p.first == str) {
            return p.second;
        }
    }
    return adt::ThreadPriority::NONE;
}

QString
instanceToString(void* ptr)
{
    std::ostringstream oss;
    if (ptr != nullptr) {
        oss << ptr;
    } else {
        oss << "nullptr";
    }
    return QString::fromStdString(oss.str());
}

} // namespace qadt
