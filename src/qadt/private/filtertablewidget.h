#ifndef QADT_FILTERTABLEWIDGET_H
#define QADT_FILTERTABLEWIDGET_H

#include "qadt/headerincludes.h"

namespace qadt
{

class FilterTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    FilterTableWidget(QWidget* parent);

public Q_SLOTS:
    void refresh();

    void onCustomMenuRequested(const QPoint& pos);

    void onDoubleClick(const QModelIndex& index);

private:
    void editFilter(const QModelIndex&);
    void deleteFilter(const QModelIndex&);

    void keyPressEvent(QKeyEvent* event) override;

private:
    enum Columns {
        MATCHING,
        THREAD_NAME,
        THREAD_PRIORITY,
        INSTANCE,
        FILE_PATH,
        FUNCTION_NAME,
        COUNT
    };
};

} // namespace qadt

#endif // QADT_FILTERTABLEWIDGET_H
