#ifndef QADT_QTMESSAGEHANDLER_H
#define QADT_QTMESSAGEHANDLER_H

#include "qadt/headerincludes.h"

void
qadtMessageHandler(QtMsgType type, const QMessageLogContext& context,
                   const QString& msg);

#endif // QADT_QTMESSAGEHANDLER_H
