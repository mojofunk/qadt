#include "qadt-private.h"

namespace qadt
{

FilterDialog::FilterDialog(const std::shared_ptr<adt::ThreadInfo>& thread_info)
    : QDialog(ApplicationPrivate::instance()->developerWindow())
{
    setWindowTitle("Add Filter");

    m_filter->thread_id = thread_info->id;
    m_filter->thread_priority
        = adt::Filter::get_thread_priority(m_filter->thread_id);

    m_filter->match_thread_id = true;

    init();
}

FilterDialog::FilterDialog(const std::shared_ptr<adt::Record>& record)
    : QDialog(ApplicationPrivate::instance()->developerWindow())
{
    setWindowTitle("Add Filter");

    m_filter->thread_id = record->thread_id;
    m_filter->thread_priority
        = adt::Filter::get_thread_priority(m_filter->thread_id);
    m_filter->instance_ptr = record->this_ptr;
    m_filter->file_path = record->src_location.file_path;
    m_filter->function_name = record->src_location.function_name;

    init();
}

FilterDialog::FilterDialog(const std::shared_ptr<adt::Filter>& p_filter)
    : QDialog(ApplicationPrivate::instance()->developerWindow())
{
    setWindowTitle("Edit Filter");

    m_filter = p_filter;

    init();
}

void
FilterDialog::init()
{
    createWidgets();
    packGrid();
    setWidgetsFromFilter();
    connectSignals();
    updateWidgetSensitivity();

    auto vLayout = new QVBoxLayout;
    vLayout->addWidget(m_gridWidget);
    vLayout->addWidget(m_buttons);
    setLayout(vLayout);
}

void
FilterDialog::createWidgets()
{
    m_gridWidget = new QWidget;

    m_buttons
        = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    m_notMatchingCheckBox = new QCheckBox("Not Matching");

    m_threadIDCheckBox = new QCheckBox("Thread ID");
    m_threadIDLabel = new QLabel;

    m_threadPriorityCheckBox = new QCheckBox("Thread priority");
    m_threadPriorityComboBox = new QComboBox;

    for (auto const& p : threadPriorityMap()) {
        m_threadPriorityComboBox->addItem(p.first);
    }

    m_instanceCheckBox = new QCheckBox("Instance");
    m_instanceLabel = new QLabel;

    m_filePathCheckBox = new QCheckBox("File Path");
    m_filePathLabel = new QLabel;

    m_functionCheckBox = new QCheckBox("Function");
    m_functionLabel = new QLabel;
}

void
FilterDialog::packGrid()
{
    auto gridLayout = new QGridLayout;
    int row = 0;
    const int columnSpan = 2;

    gridLayout->addWidget(m_notMatchingCheckBox, row, 0);
    row++;

    gridLayout->addWidget(m_threadIDCheckBox, row, 0);
    gridLayout->addWidget(m_threadIDLabel, row, 1, 1, columnSpan);
    row++;

    gridLayout->addWidget(m_threadPriorityCheckBox, row, 0);
    gridLayout->addWidget(m_threadPriorityComboBox, row, 1, 1, columnSpan);
    row++;

    gridLayout->addWidget(m_instanceCheckBox, row, 0);
    gridLayout->addWidget(m_instanceLabel, row, 1, 1, columnSpan);
    row++;

    gridLayout->addWidget(m_filePathCheckBox, row, 0);
    gridLayout->addWidget(m_filePathLabel, row, 1, 1, columnSpan);
    row++;

    gridLayout->addWidget(m_functionCheckBox, row, 0);
    gridLayout->addWidget(m_functionLabel, row, 1, 1, columnSpan);
    row++;

    gridLayout->setColumnStretch(1, 10);
    gridLayout->setColumnStretch(2, 20);
    gridLayout->setContentsMargins(0, 0, 0, 0);

    m_gridWidget->setLayout(gridLayout);
}

void
FilterDialog::updateWidgetSensitivity()
{
    m_threadIDLabel->setEnabled(m_filter->match_thread_id);
    m_threadPriorityComboBox->setEnabled(m_filter->match_thread_priority);
    m_instanceLabel->setEnabled(m_filter->match_instance);
    m_filePathLabel->setEnabled(m_filter->match_file_path);
    m_functionLabel->setEnabled(m_filter->match_function);

    m_buttons->button(QDialogButtonBox::Ok)->setEnabled(m_filter->enabled());
}

void
FilterDialog::setWidgetsFromFilter()
{
    m_notMatchingCheckBox->setChecked(m_filter->not_matching);

    m_threadIDCheckBox->setChecked(m_filter->match_thread_id);
    m_threadIDLabel->setText(threadIdToName(m_filter->thread_id));

    m_threadPriorityCheckBox->setChecked(m_filter->match_thread_priority);
    m_threadPriorityComboBox->setCurrentText(
        threadPriorityToString(m_filter->thread_priority));

    m_instanceCheckBox->setChecked(m_filter->match_instance);
    if (m_filter->instance_ptr == nullptr) {
        m_instanceCheckBox->setEnabled(false);
    } else {
        m_instanceLabel->setText(instanceToString(m_filter->instance_ptr));
    }

    m_filePathCheckBox->setChecked(m_filter->match_file_path);
    if (m_filter->file_path) {
        m_filePathLabel->setText(QString::fromStdString(m_filter->file_path));
    } else {
        m_filePathCheckBox->hide();
        m_filePathLabel->hide();
    }

    m_functionCheckBox->setChecked(m_filter->match_function);
    if (m_filter->function_name) {
        m_functionLabel->setText(QString::fromStdString(
            adt::short_function_name(m_filter->function_name)));
    } else {
        m_functionCheckBox->hide();
        m_functionLabel->hide();
    }
}

void
FilterDialog::updateFilterFromWidgets()
{
    m_filter->not_matching = m_notMatchingCheckBox->isChecked();
    m_filter->match_thread_id = m_threadIDCheckBox->isChecked();
    m_filter->match_thread_priority = m_threadPriorityCheckBox->isChecked();

    auto priorityStr = m_threadPriorityComboBox->currentText();
    m_filter->thread_priority = stringToThreadPriority(priorityStr);

    m_filter->match_instance = m_instanceCheckBox->isChecked();
    m_filter->match_file_path = m_filePathCheckBox->isChecked();
    m_filter->match_function = m_functionCheckBox->isChecked();

    updateWidgetSensitivity();
}

void
FilterDialog::onCheckBoxToggled(bool)
{
    updateFilterFromWidgets();
}

void
FilterDialog::onComboBoxTextChanged(const QString&)
{
    updateFilterFromWidgets();
}

void
FilterDialog::connectSignals()
{
    connect(m_notMatchingCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_threadIDCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_threadPriorityCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_threadPriorityComboBox, &QComboBox::currentTextChanged, this,
            &FilterDialog::onComboBoxTextChanged);

    connect(m_instanceCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_filePathCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_functionCheckBox, &QCheckBox::stateChanged, this,
            &FilterDialog::onCheckBoxToggled);

    connect(m_buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

} // namespace qadt
