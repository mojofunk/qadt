#ifndef QADT_SOURCEINCLUDES_H
#define QADT_SOURCEINCLUDES_H

/**
 * @file
 *
 * These are all the header includes required by all source(cpp) files
 * in the qadt module. Includes in source files are treated in a module
 * like manner where the total includes required by a module defined in
 * a single location rather on a source by source file basis.
 */

#include <iostream>
#include <sstream>

#include <QtCore/QDir>

#include <QtGlobal>
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#include <QtGui/QAction>
#else
#include <QtWidgets/QAction>
#endif

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QVBoxLayout>

#include <QtGui/QTextBlock>

#endif // QADT_SOURCEINCLUDES_H
