#ifndef QADT_LOGCATEGORYCONTROL_H
#define QADT_LOGCATEGORYCONTROL_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogCategoryListWidget;

class LogCategoryControl : public QWidget
{
    Q_OBJECT

public:
    LogCategoryControl();

public:
    QSize sizeHint() const;
};

} // namespace qadt

#endif // QADT_LOGCATEGORYCONTROL_H
