#ifndef QADT_LOGRECORDTABLEVIEW_H
#define QADT_LOGRECORDTABLEVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogRecordTableModel;

class LogRecordTableView : public QTableView
{
    Q_OBJECT

public:
    LogRecordTableView();

    std::shared_ptr<adt::Record> get_selected_record() const;

    std::vector<std::shared_ptr<adt::Record>> get_visible_records() const;

Q_SIGNALS:
    void signalSelectedRecordChanged(std::shared_ptr<adt::Record>) const;

public Q_SLOTS:
    void setRecordFilter(const QString& string);

private Q_SLOTS:
    void onSelectionChanged(const QItemSelection& selected,
                            const QItemSelection& deselected) const;

    void onDoubleClick(const QModelIndex& index);

private:
    QSortFilterProxyModel* m_proxyModel;
    LogRecordTableModel* m_model;
};

} // namespace qadt

#endif // QADT_LOGRECORDTABLEVIEW_H
