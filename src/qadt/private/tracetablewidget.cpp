#include "qadt-private.h"

namespace qadt
{

TraceTableWidget::TraceTableWidget(QWidget* parent)
    : QTableWidget(parent)
{
    setColumnCount(Columns::COUNT);
    QStringList columns;
    columns << "Frame"
            << "Address"
            << "Function"
            << "Location";
    setHorizontalHeaderLabels(columns);

    setEditTriggers(QAbstractItemView::NoEditTriggers);

    verticalHeader()->hide();
    setShowGrid(false);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);

    horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    horizontalHeader()->setStretchLastSection(true);

    connect(this, &QTableWidget::itemSelectionChanged, this,
            &TraceTableWidget::onSelectionChanged);
}

void
TraceTableWidget::setRecord(std::shared_ptr<adt::Record> record)
{
    setRowCount(0);

    if (!record || !record->stack_trace) {
        return;
    }

    auto const stack_frames = record->stack_trace->get_stack_frames();

    for (auto const& frame : stack_frames) {
        auto const newRow = rowCount();
        insertRow(newRow);
        auto item = new QTableWidgetItem(QString::number(newRow));
        QVariant vInfo;
        vInfo.setValue(frame);
        item->setData(Qt::UserRole, vInfo);
        setItem(newRow, Columns::FRAME, item);

        // address_to_string()
        std::ostringstream oss;
        oss << frame->get_address();
        QString qstr = QString::fromStdString(oss.str());

        setItem(newRow, Columns::ADDRESS, new QTableWidgetItem(qstr));

        qstr = QString::fromStdString(frame->get_function_name());
        setItem(newRow, Columns::FUNCTION, new QTableWidgetItem(qstr));

        std::string location;
        if (frame->has_file_name()) {
            location = frame->get_file_name();

            if (frame->has_line_number()) {
                location += ":";
                location += std::to_string(frame->get_line_number());
            }
        }
        qstr = QString::fromStdString(location);
        setItem(newRow, Columns::LOCATION, new QTableWidgetItem(qstr));
    }
}

void
TraceTableWidget::onSelectionChanged()
{
    QModelIndexList rowIndexList = selectionModel()->selectedRows();

    std::shared_ptr<adt::StackFrame> frame_sptr;

    if (!rowIndexList.empty()) {
        QModelIndex const index = rowIndexList.front();
        QVariant vFrame = item(index.row(), Columns::FRAME)->data(Qt::UserRole);
        frame_sptr = qvariant_cast<std::shared_ptr<adt::StackFrame>>(vFrame);
    }
    signalSelectedStackFrameChanged(frame_sptr);
}

} // namespace qadt
