#include "qadt-private.h"

namespace qadt
{

LogControlBar::LogControlBar()
{
    auto hlayout = new QHBoxLayout;

    m_recordButton = new QPushButton();
    m_recordButton->setCheckable(true);
    m_recordButton->setChecked(ApplicationPrivate::instance()->recordingEnabled());
    onRecordChanged();
    connect(m_recordButton, &QAbstractButton::toggled, this, &LogControlBar::onRecordChanged);
    hlayout->addWidget(m_recordButton);

    auto b = new QPushButton("Auto Update");
    b->setCheckable(true);
    connect(b, &QAbstractButton::toggled, this, [=](bool checked) {
        ApplicationPrivate::instance()->setAutoUpdate(checked);
    });
    hlayout->addWidget(b);

    b = new QPushButton("Stack Traces");
    b->setCheckable(true);
    b->setChecked(adt::Log::get_trace_enabled());
    connect(b, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_trace_enabled(checked); });
    hlayout->addWidget(b);

    b = new QPushButton("Sync");
    b->setCheckable(true);
    b->setChecked(adt::Log::get_sync_enabled());
    connect(b, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_sync_enabled(checked); });
    hlayout->addWidget(b);

    b = new QPushButton("Properties");
    b->setCheckable(true);
    b->setChecked(adt::Log::get_properties_enabled());
    connect(b, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_properties_enabled(checked); });
    hlayout->addWidget(b);

    m_filtersButton = new QPushButton("Filter");
    m_filtersButton->setCheckable(true);
    connect(m_filtersButton, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_filters_enabled(checked); });
    hlayout->addWidget(m_filtersButton);

    connect(ApplicationPrivate::instance(),
            &ApplicationPrivate::signalFiltersChanged, this,
            &LogControlBar::onFiltersChanged);

    // set the state of the button to reflect internal adt state
    onFiltersChanged();

    m_pauseButton = new QPushButton("Pause");
    m_pauseButton->setCheckable(true);
    m_pauseButton->setChecked(false);
    connect(m_pauseButton, &QAbstractButton::toggled, this, &LogControlBar::onPauseChanged);
    hlayout->addWidget(m_pauseButton);

    m_messagesButton = new QPushButton("Messages");
    m_messagesButton->setCheckable(true);
    m_messagesButton->setChecked(adt::Log::get_message_type_enabled());
    connect(m_messagesButton, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_message_type_enabled(checked); });
    hlayout->addWidget(m_messagesButton);

    m_functionButton = new QPushButton("Function Calls");
    m_functionButton->setCheckable(true);
    m_functionButton->setChecked(adt::Log::get_function_type_enabled());
    connect(m_functionButton, &QAbstractButton::toggled, this, [=](bool checked) {
        adt::Log::set_function_type_enabled(checked);
    });
    hlayout->addWidget(m_functionButton);

    m_dataButton = new QPushButton("Data");
    m_dataButton->setCheckable(true);
    m_dataButton->setChecked(adt::Log::get_data_type_enabled());
    connect(m_dataButton, &QAbstractButton::toggled, this,
            [=](bool checked) { adt::Log::set_data_type_enabled(checked); });
    hlayout->addWidget(m_dataButton);

    m_classButton = new QPushButton("Class Allocation");
    m_classButton->setCheckable(true);
    m_classButton->setChecked(adt::Log::get_allocation_type_enabled());
    connect(m_classButton, &QAbstractButton::toggled, this, [=](bool checked) {
        adt::Log::set_allocation_type_enabled(checked);
    });
    hlayout->addWidget(m_classButton);

    m_globalButton = new QPushButton("Global Allocation");
    m_globalButton->setCheckable(true);
    m_globalButton->setChecked(adt::GlobalAllocator::get_logging_enabled());
    connect(m_globalButton, &QAbstractButton::toggled, this, [=](bool checked) {
        adt::GlobalAllocator::set_logging_enabled(checked);
    });
    hlayout->addWidget(m_globalButton);

    setLayout(hlayout);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void
LogControlBar::onRecordChanged()
{
    bool checked = m_recordButton->isChecked();

    ApplicationPrivate::instance()->setRecordingEnabled(checked);

    if (checked) {
        m_recordButton->setText("Stop Trace");
    } else {
        m_recordButton->setText("Record Trace");
    }
}

void
LogControlBar::onFiltersChanged()
{
    auto const filters = adt::Log::get_filters();

    if (filters.empty()) {
        m_filtersButton->setChecked(false);
        m_filtersButton->setEnabled(false);
    } else {
        if (!m_filtersButton->isEnabled()) {
            m_filtersButton->setEnabled(true);
            // Enable filters when first filter added
            m_filtersButton->setChecked(true);
        }
    }
}

void
LogControlBar::onPauseChanged()
{
    bool enable = !m_pauseButton->isChecked();

    if (enable) {
        adt::Log::set_message_type_enabled(m_messagesButton->isChecked());
        adt::Log::set_function_type_enabled(m_functionButton->isChecked());
        adt::Log::set_data_type_enabled(m_dataButton->isChecked());
        adt::Log::set_allocation_type_enabled(m_classButton->isChecked());
        adt::GlobalAllocator::set_logging_enabled(m_globalButton->isChecked());
    } else {
        adt::Log::set_message_type_enabled(false);
        adt::Log::set_function_type_enabled(false);
        adt::Log::set_data_type_enabled(false);
        adt::Log::set_allocation_type_enabled(false);
        adt::GlobalAllocator::set_logging_enabled(false);
    }
    m_messagesButton->setEnabled(enable);
    m_functionButton->setEnabled(enable);
    m_dataButton->setEnabled(enable);
    m_classButton->setEnabled(enable);
    m_globalButton->setEnabled(enable);
}

} // namespace qadt
