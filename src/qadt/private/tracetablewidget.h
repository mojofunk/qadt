#ifndef QADT_TRACETABLEWIDGET_H
#define QADT_TRACETABLEWIDGET_H

#include "qadt/headerincludes.h"

namespace qadt
{

class TraceTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    TraceTableWidget(QWidget* parent);

public Q_SLOTS:
    void setRecord(std::shared_ptr<adt::Record>);

    void onSelectionChanged();

Q_SIGNALS:
    void
        signalSelectedStackFrameChanged(std::shared_ptr<adt::StackFrame>) const;

private:
    enum Columns { FRAME, ADDRESS, FUNCTION, LOCATION, COUNT };
};

} // namespace qadt

#endif // QADT_TRACETABLEWIDGET_H
