#ifndef QADT_SOURCEVIEW_H
#define QADT_SOURCEVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class SourceView : public QWidget
{
    Q_OBJECT

public:
    SourceView();
    virtual ~SourceView() = default;

public Q_SLOTS:
    void setRecord(std::shared_ptr<adt::Record>);

    void setStackFrame(std::shared_ptr<adt::StackFrame>);

private:
    void set_file(QString const& filePath);

    void set_line_number(int const lineNumber);

private:
    QPlainTextEdit* m_textEdit;
};

} // namespace qadt

#endif // QADT_SOURCEVIEW_H
