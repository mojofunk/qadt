#ifndef QADT_QTFILE_H
#define QADT_QTFILE_H

#include "qadt/headerincludes.h"

class QtFile : public adt::File
{
public:
    QtFile(QString const& file_path);

    ~QtFile() override;

public: // adt::File interface
    bool open() override;

    bool close() override;

    bool write(std::string const& str) override;

private: // Data
    QFile m_file;
};

#endif // QADT_QTFILE_H
