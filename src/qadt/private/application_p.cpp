#include "qadt-private.h"

namespace qadt
{

ApplicationPrivate*
ApplicationPrivate::instance()
{
    static ApplicationPrivate* s_instance = new ApplicationPrivate();
    return s_instance;
}

ApplicationPrivate::ApplicationPrivate()
    : m_updateTimer()
    , m_maxRecords(4096)
    , m_recordQueue(4096)
    , m_sink(new adt::TraceEventSink)
{
    m_updateTimer.setInterval(1000);

    setAutoUpdate(false);

    adt::Log::set_cache_handler(this);
    adt::Log::set_enabled();
    adt::Log::set_sink(m_sink);

    connect(&m_updateTimer, &QTimer::timeout, this,
            &ApplicationPrivate::onUpdateTimer);

    qDebug() << "qadt : Install Qt Message handler: ";

    qInstallMessageHandler(qadtMessageHandler);
}

ApplicationPrivate::~ApplicationPrivate()
{
    // reinstate default writer function
    // qInstallMessageHandler(0);
    adt::Log::set_enabled(false);
    adt::Log::reset_cache_handler();
}

bool
ApplicationPrivate::recordingEnabled()
{
    std::shared_ptr<adt::Sink> sink = adt::Log::get_sink();

    if (sink) {
        return sink->get_enabled();
    }
    return false;
}

void
ApplicationPrivate::setRecordingEnabled(bool enable)
{
    std::shared_ptr<adt::Sink> sink = adt::Log::get_sink();
    sink->set_enabled(enable);
    signalRecordingChanged();
}

void
ApplicationPrivate::clearRecords()
{
    m_recordList.clear();
    signalRecordsChanged();
}

void
ApplicationPrivate::setPresetEnabled(std::shared_ptr<adt::Preset> preset)
{
    adt::Log::set_preset_enabled(preset);
    signalPresetsChanged();
}

void
ApplicationPrivate::setPresetDisabled(std::shared_ptr<adt::Preset> preset)
{
    adt::Log::set_preset_disabled(preset);
    signalPresetsChanged();
}

void
ApplicationPrivate::setSourceSearchPaths(std::vector<std::string> const& paths)
{
    m_sourceSearchPaths = paths;
}

std::vector<std::string>
ApplicationPrivate::sourceSearchPaths()
{
    return m_sourceSearchPaths;
}

void
ApplicationPrivate::setAutoUpdate(bool enable)
{
    if (enable && m_updateTimer.isActive()) {
        return;
    }

    if (enable) {
        m_updateTimer.start();
    } else {
        m_updateTimer.stop();
    }
}

bool
ApplicationPrivate::parseCommandLine(int* argc, char*** argv)
{

#if 0
    if (!LibraryOptions::parseCommandLine(argc, argv)) {
      return false;
    }
#endif

    setLogConfigFromLibraryOptions();
    return true;
}

void
ApplicationPrivate::setLogConfigFromLibraryOptions()
{
    auto path = LibraryOptions::logFilePath();

    if (path.isEmpty()) {
        // set a default output file path

        QString defaultFilename = QCoreApplication::applicationName();

        if (defaultFilename.isEmpty()) {
            defaultFilename = "qadt";
        }
        defaultFilename += ".trace.json";
        qDebug() << "qadt default output file: " << defaultFilename;
        path = QDir::home().filePath(defaultFilename);
    }

    std::shared_ptr<adt::File> logFile(new adt::StdFile(path.toStdString()));

    m_sink->set_output_file(logFile);

    LibraryOptions::enableTrace();

    if (LibraryOptions::enableCategories()) {
        m_sink->set_enabled(true);
    }

    LibraryOptions::enableRecordTypes();
}

DeveloperWindow*
ApplicationPrivate::developerWindow()
{
    if (!m_developerWindow) {
        m_developerWindow.reset(new DeveloperWindow);
    }
    return m_developerWindow.get();
}

void
ApplicationPrivate::addFilter(std::shared_ptr<adt::Filter> filter)
{
    adt::Log::add_filter(std::move(filter));
    signalFiltersChanged();
}

void
ApplicationPrivate::removeFilter(std::shared_ptr<adt::Filter> const& filter)
{
    adt::Log::remove_filter(filter);
    signalFiltersChanged();
}

void
ApplicationPrivate::onUpdateTimer()
{
    if (m_recordsChanged) {
        // signal if records have changed since the last timer event
        signalRecordsChanged();
        m_recordsChanged = false;
    }
}

void
ApplicationPrivate::handle_new_records(adt::RecordSPVec const& new_records)
{
    for (auto const& record : new_records) {
        // Only fails if allocation fails.
        m_recordQueue.enqueue(record);
    }

    // TODO Use new syntax when min QT > 5.10.0
    QMetaObject::invokeMethod(this, "onNewRecords", Qt::QueuedConnection);
}

void
ApplicationPrivate::onNewRecords()
{
    std::shared_ptr<adt::Record> new_record;

    while (m_recordQueue.try_dequeue(new_record)) {
        m_recordList.push_back(new_record);

        while (m_recordList.size() > maxRecords()) {
            m_recordList.pop_front();
        }
    }

    m_recordsChanged = true;
};

std::size_t
ApplicationPrivate::preferred_cache_size()
{
    return m_maxRecords;
}

} // namespace qadt
