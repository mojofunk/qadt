#include "qadt-private.h"

namespace qadt
{

LogRecordTableModel::LogRecordTableModel(QObject* parent)
    : QAbstractTableModel(parent)
{
    connect(ApplicationPrivate::instance(),
            &ApplicationPrivate::signalRecordsChanged, this,
            &LogRecordTableModel::refresh);
}

int
LogRecordTableModel::rowCount(const QModelIndex& parent) const
{
    return static_cast<int>(m_records.size());
}

int
LogRecordTableModel::columnCount(const QModelIndex& parent) const
{
    return Columns::COUNT;
}

QVariant
LogRecordTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
        auto const& record = m_records[index.row()];
        switch (index.column()) {
        case Columns::CATEGORY:
            return record->category->name;
        case Columns::RECORD_TYPE:
            return adt::RecordTypeNames::get_name(record->type);
        case Columns::MESSAGE:
            return messageStringFromRecord(*record);
        case Columns::THREAD_ID:
            return threadIdToName(*record);
        case Columns::DURATION:
            return static_cast<qulonglong>(record->timing.duration());
        case Columns::FUNCTION_NAME:
            return record->src_location.function_name;
        case Columns::LINE:
            return record->src_location.line;
        case Columns::FILE_PATH:
            return record->src_location.file_path;
        }
    }

    return QVariant();
}

#if 0
Qt::ItemFlags LogRecordTableModel::flags(const QModelIndex& index) const
{
    if (index.isValid()) {
        if (index.column() == 1) {
            return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable
                | Qt::ItemIsEditable;
        }
    }
    return Qt::NoItemFlags;
}
#endif

QVariant
LogRecordTableModel::headerData(int section, Qt::Orientation orientation,
                                   int role) const
{
    if (orientation == Qt::Vertical || role != Qt::DisplayRole) {
        return QVariant();
    }

    switch (section) {
    case Columns::CATEGORY:
        return "Category";
    case Columns::RECORD_TYPE:
        return "Record Type";
    case Columns::MESSAGE:
        return "Message";
    case Columns::THREAD_ID:
        return "Thread ID";
    case Columns::DURATION:
        return "Duration(us)";
    case Columns::FUNCTION_NAME:
        return "Function Name";
    case Columns::LINE:
        return "Line";
    case Columns::FILE_PATH:
        return "File";
    }
    return QVariant();
}

std::shared_ptr<adt::Record>
LogRecordTableModel::recordAtIndex(const QModelIndex& index) const
{
    // Can this happen??
    if (!index.isValid()) {
        return std::shared_ptr<adt::Record>();
    }
    return m_records[index.row()];
}

void
LogRecordTableModel::refresh()
{
    beginResetModel();

    auto const& record_list = ApplicationPrivate::instance()->records();

    m_records.clear();
    m_records.reserve(record_list.size());

    for (auto& record : record_list) {
        m_records.push_back(record);
    }

    endResetModel();
}

QString
LogRecordTableModel::messageStringFromRecord(const adt::Record& record) const
{
    auto const max_message_size = 120;

    std::string message_string = record.properties_to_string();

    if (message_string.length() > max_message_size) {
        message_string.resize(max_message_size);
        message_string += "...";
    }

    return QString::fromStdString(message_string);
}

QString
LogRecordTableModel::threadIdToName(const adt::Record& record) const
{
    return qadt::threadIdToName(record.thread_id);
}

} // namespace qadt
