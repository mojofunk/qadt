#ifndef QADT_LOGRECORDLISTVIEW_H
#define QADT_LOGRECORDLISTVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogRecordTableView;
class SearchLineEdit;

class LogRecordListView : public QWidget
{
    Q_OBJECT

public:
    LogRecordListView();

public:
    // Access to signalSelectedRecordChanged()
    LogRecordTableView* tableView() { return m_tableView; }

public Q_SLOTS:
    void onClearButtonClicked();
    void onExportCSVButtonClicked();

private:
    LogRecordTableView* m_tableView;
};

} // namespace qadt

#endif // QADT_LOGRECORDLISTVIEW_H
