#include "qadt-private.h"

namespace qadt
{

TraceView::TraceView()
	: m_traceTableWidget(new TraceTableWidget(this))
{
    auto vlayout = new QVBoxLayout;

    vlayout->addWidget(m_traceTableWidget);

    setLayout(vlayout);
}

} // namespace qadt
