#include "qadt-private.h"

namespace qadt
{

LogRecordTableView::LogRecordTableView()
    : m_proxyModel(new QSortFilterProxyModel)
    , m_model(new LogRecordTableModel(this))
{
    m_proxyModel->setSourceModel(m_model);
    m_proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setFilterKeyColumn(-1); // match all columns

    setModel(m_proxyModel);
    setShowGrid(false);
    verticalHeader()->hide();
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    horizontalHeader()->setStretchLastSection(true);

    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this,
            &LogRecordTableView::onSelectionChanged);

    connect(this, &QTableView::doubleClicked, this,
            &LogRecordTableView::onDoubleClick);
}

std::shared_ptr<adt::Record>
LogRecordTableView::get_selected_record() const
{
    std::shared_ptr<adt::Record> record;

    auto rowIndexList = selectionModel()->selectedRows();

    if (!rowIndexList.empty()) {
        auto const proxy_index = rowIndexList.front();

        auto source_index = m_proxyModel->mapToSource(proxy_index);

        // It has to be valid as the proxy is a filtered subset?
        assert(source_index.isValid());

        record = m_model->recordAtIndex(source_index);
    }

    return record;
}

std::vector<std::shared_ptr<adt::Record>>
LogRecordTableView::get_visible_records() const
{
    std::vector<std::shared_ptr<adt::Record>> visible_records;

    for (int row = 0; row < m_model->rowCount(); ++row) {
        auto const index = m_model->index(row, 0);

        if (m_proxyModel->mapFromSource(index).isValid()) {
            visible_records.push_back(m_model->recordAtIndex(index));
        }
    }
    return visible_records;
}

void
LogRecordTableView::onSelectionChanged(const QItemSelection& selected,
                                      const QItemSelection& deselected) const
{
    signalSelectedRecordChanged(get_selected_record());
}

void
LogRecordTableView::onDoubleClick(const QModelIndex& index)
{
    QModelIndex const source_index = m_proxyModel->mapToSource(index);

    assert(source_index.isValid());

    auto record = m_model->recordAtIndex(source_index);

    if (!record) {
        return;
    }

    FilterDialog dialog(record);

    if (dialog.exec() == QDialog::Accepted) {
        ApplicationPrivate::instance()->addFilter(dialog.filter());
    }
}


void
LogRecordTableView::setRecordFilter(const QString& str)
{
    m_proxyModel->setFilterFixedString(str);
}

} // namespace qadt
