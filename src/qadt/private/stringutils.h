#ifndef QADT_STRINGUTILS_H
#define QADT_STRINGUTILS_H

#include "qadt/headerincludes.h"

namespace qadt
{

QString
threadIdToName(const std::thread::id&);

QString
threadIdToString(const std::thread::id&);

using ThreadPriorityMap = std::map<QString, adt::ThreadPriority>;

ThreadPriorityMap
threadPriorityMap();

QString
threadPriorityToString(const adt::ThreadPriority&);

adt::ThreadPriority
stringToThreadPriority(const QString&);

QString
instanceToString(void* ptr);

} // namespace qadt

#endif // QADT_STRINGUTILS_H
