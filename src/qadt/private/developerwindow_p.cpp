#include "qadt-private.h"

namespace qadt
{

DeveloperWindowPrivate::DeveloperWindowPrivate()
{
    auto vlayout = new QVBoxLayout;

    vlayout->addWidget(new LogControlBar());

    auto hSplitterTop = new QSplitter(Qt::Horizontal);

    auto recordListView = new LogRecordListView();

    hSplitterTop->addWidget(new LogCategoryControl());
    hSplitterTop->addWidget(recordListView);

    auto sourceView = new SourceView();

    auto tabWidgetLeft = new QTabWidget();
    tabWidgetLeft->addTab(sourceView, "Source View");

    auto filterView = new FilterListView();
    tabWidgetLeft->addTab(filterView, "Filter View");

    auto threadView = new ThreadListView();
    tabWidgetLeft->addTab(threadView, "Thread View");

    auto classView = new ClassListView();
    tabWidgetLeft->addTab(classView, "Class View");

    auto hSplitterBottom = new QSplitter(Qt::Horizontal);

    hSplitterBottom->addWidget(tabWidgetLeft);

    auto tabWidgetRight = new QTabWidget();

    auto traceView = new TraceView();
    auto propertyView = new PropertyListView();

    tabWidgetRight->addTab(traceView, "Trace View");
    tabWidgetRight->addTab(propertyView, "Property View");

    hSplitterBottom->addWidget(tabWidgetRight);

    auto vSplitter = new QSplitter(Qt::Vertical, this);

    vSplitter->addWidget(hSplitterTop);
    vSplitter->addWidget(hSplitterBottom);

    vlayout->addWidget(vSplitter);

    connect(recordListView->tableView(),
            &LogRecordTableView::signalSelectedRecordChanged, sourceView,
            &SourceView::setRecord);

    connect(recordListView->tableView(),
            &LogRecordTableView::signalSelectedRecordChanged,
            traceView->traceTableWidget(),
            &TraceTableWidget::setRecord);

    connect(recordListView->tableView(),
            &LogRecordTableView::signalSelectedRecordChanged, propertyView,
            &PropertyListView::setRecord);

    connect(traceView->traceTableWidget(),
            &TraceTableWidget::signalSelectedStackFrameChanged, sourceView,
            &SourceView::setStackFrame);

    setLayout(vlayout);
}

} // namespace qadt
