#ifndef QADT_SEARCHLINEEDIT_H
#define QADT_SEARCHLINEEDIT_H

#include "qadt/headerincludes.h"

namespace qadt
{

class SearchLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    SearchLineEdit() = default;

    void keyPressEvent(QKeyEvent* event) override;
};

} // namespace qadt

#endif // QADT_SEARCHLINEEDIT_H
