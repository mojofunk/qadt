#include "qadt-private.h"

namespace LOG
{

A_DEFINE_LOG_CATEGORY(QDebug, "Qt::Debug");
A_DEFINE_LOG_CATEGORY(QInfo, "Qt::Info");
A_DEFINE_LOG_CATEGORY(QWarning, "Qt::Warning");
A_DEFINE_LOG_CATEGORY(QCritical, "Qt::Critical");
A_DEFINE_LOG_CATEGORY(QFatal, "Qt::Fatal");

} // namespace LOG

static adt::LogCategory*
msgTypeToLogCategory(QtMsgType type)
{
    switch (type) {
    case QtDebugMsg:
        return LOG::QDebug;
        break;
    case QtInfoMsg:
        return LOG::QInfo;
        break;
    case QtWarningMsg:
        return LOG::QWarning;
        break;
    case QtCriticalMsg:
        return LOG::QCritical;
        break;
    case QtFatalMsg:
        return LOG::QFatal;
        break;
    }
    return LOG::QFatal;
}

void
qadtMessageHandler(QtMsgType type, const QMessageLogContext& context,
                   const QString& msg)
{

    QByteArray localMsg = msg.toLocal8Bit();

    auto log_category = msgTypeToLogCategory(type);

    if (!log_category->enabled || !log_category->message_type_enabled) {
        return;
    }

    adt::MessageLogger logger(log_category, context.line, context.file,
                              context.function);

    if (logger.get_enabled()) {
        logger.log_record->move_to_property_1(adt::RecordTypeNames::message,
                                              localMsg.constData());
    }
}
