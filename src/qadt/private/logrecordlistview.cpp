#include "qadt-private.h"

namespace qadt
{

LogRecordListView::LogRecordListView()
    : m_tableView(new LogRecordTableView())
{
    auto searchLineEdit = new SearchLineEdit();

    searchLineEdit->setClearButtonEnabled(true);
    searchLineEdit->setPlaceholderText("Search Records...");

    auto ExportCSVButton = new QPushButton("Export to CSV file");
    auto clearRecordsButton = new QPushButton("Clear Records");

    auto hLayout = new QHBoxLayout();

    hLayout->addWidget(searchLineEdit);
    hLayout->addWidget(ExportCSVButton);
    hLayout->addWidget(clearRecordsButton);

    hLayout->setContentsMargins(0, 0, 0, 0);

    auto hWidget = new QWidget(this);
    hWidget->setLayout(hLayout);

    auto vLayout = new QVBoxLayout();
    vLayout->addWidget(hWidget);
    vLayout->addWidget(m_tableView);

    setLayout(vLayout);

    connect(searchLineEdit, &QLineEdit::textChanged, m_tableView,
            &LogRecordTableView::setRecordFilter);

    connect(clearRecordsButton, &QAbstractButton::clicked, this,
            &LogRecordListView::onClearButtonClicked);

    connect(ExportCSVButton, &QAbstractButton::clicked, this,
            &LogRecordListView::onExportCSVButtonClicked);
}

void
LogRecordListView::onClearButtonClicked()
{
    ApplicationPrivate::instance()->clearRecords();
}

void
LogRecordListView::onExportCSVButtonClicked()
{
    auto record = m_tableView->get_selected_record();
    auto const visible_records = m_tableView->get_visible_records();

    QString record_used = "selected";
    bool use_selected = true;

    if (!record && !visible_records.empty()) {
        record = visible_records.front();
        use_selected = false;
        record_used = "first record";
    }

    const char* const error_window_title = "Failed to Export CSV File";

    if (!record) {
        QMessageBox::information(Application::developerWindow(),
                                 error_window_title,
                                 "There are no records to export.");
        return;
    }

    if (record->properties.empty()) {
        QString message = "The " + record_used + " has no properties to use as CSV template.";
        QMessageBox::warning(Application::developerWindow(), error_window_title, message);
        return;
    }

    ExportCSVDialog dialog(record, use_selected);

    if (dialog.exec() != QDialog::Accepted) {
        return;
    }

    QString file_path = QDir::home().filePath("qadt.csv");

    QtFile file(file_path);

    if (!adt::write_records_to_csv_file(file, visible_records,
                                        dialog.options())) {
        QMessageBox::warning(Application::developerWindow(), error_window_title,
                             "Writing to the CSV file failed.");
    }
}

} // namespace qadt
