#ifndef QADT_LOGCONTROLBAR_H
#define QADT_LOGCONTROLBAR_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogControlBar : public QWidget
{
    Q_OBJECT

public:
    LogControlBar();

private Q_SLOTS:
    void onRecordChanged();
    void onFiltersChanged();
    void onPauseChanged();

private: // member data
    QPushButton* m_recordButton;

    QPushButton* m_filtersButton;

    QPushButton* m_pauseButton;

    QPushButton* m_messagesButton;
    QPushButton* m_functionButton;
    QPushButton* m_dataButton;
    QPushButton* m_classButton;
    QPushButton* m_globalButton;
};

} // namespace qadt

#endif // QADT_LOGCONTROLBAR_H
