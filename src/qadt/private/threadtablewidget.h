#ifndef QADT_THREADTABLEWIDGET_H
#define QADT_THREADTABLEWIDGET_H

#include "qadt/headerincludes.h"

namespace qadt
{

class ThreadTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    ThreadTableWidget(QWidget* parent);

public Q_SLOTS:
    void refresh();

private Q_SLOTS:
    void onCustomMenuRequested(const QPoint& pos);

    void onDoubleClick(const QModelIndex& index);

private:
    void addFilter(const QModelIndex&);

private:
    enum Columns { ID, NAME, PRIORITY, COUNT };
};

} // namespace qadt

#endif // QADT_THREADTABLEWIDGET_H
