#ifndef QADT_APPLICATION_P_H
#define QADT_APPLICATION_P_H

#include "qadt/headerincludes.h"

namespace qadt
{

class DeveloperWindow;

class ApplicationPrivate : public QObject, private adt::RecordCacheHandler
{
    Q_OBJECT

public:
    static ApplicationPrivate* instance();

    ~ApplicationPrivate();

private:
    ApplicationPrivate();

public:
    const std::list<std::shared_ptr<adt::Record>>& records() const
    {
        return m_recordList;
    }

    uint32_t maxRecords() const { return m_maxRecords; }

    // TODO set max log records
    void setMaxRecords(uint32_t new_max) { m_maxRecords = new_max; }

    bool recordingEnabled();

    void setRecordingEnabled(bool enable);

    void clearRecords();

public: // adt::Presets
    void addPreset(std::shared_ptr<adt::Preset>);

    void removePreset(std::shared_ptr<adt::Preset> const&);

    void setPresetEnabled(std::shared_ptr<adt::Preset> preset);

    void setPresetDisabled(std::shared_ptr<adt::Preset> preset);

public:
    void setSourceSearchPaths(std::vector<std::string> const& paths);

    std::vector<std::string> sourceSearchPaths();

    void setAutoUpdate(bool enable);

    QTimer& autoUpdateTimer() { return m_updateTimer; }

public:
    bool parseCommandLine(int* argc, char*** argv);

    void setLogConfigFromLibraryOptions();

    DeveloperWindow* developerWindow();

    void addFilter(std::shared_ptr<adt::Filter> filter);

    void removeFilter(std::shared_ptr<adt::Filter> const& filter);

private Q_SLOTS:
    void onUpdateTimer();

    void onNewRecords();

Q_SIGNALS:
    void signalAutoUpdate();

    void signalRecordingChanged();

    void signalRecordsChanged();

    void signalFiltersChanged();

    void signalPresetsChanged();

private: // RecordCacheHandler
    void handle_new_records(adt::RecordSPVec const&) override;

    std::size_t preferred_cache_size() override;

private: // Data
    std::vector<std::string> m_sourceSearchPaths;

    QTimer m_updateTimer;

    uint32_t m_maxRecords;

    bool m_recordsChanged{ false };

    adt::RecordQueue m_recordQueue;

    std::list<std::shared_ptr<adt::Record>> m_recordList;

    std::unique_ptr<DeveloperWindow> m_developerWindow;

    std::shared_ptr<adt::TraceEventSink> m_sink;
};

} // namespace qadt

#endif // QADT_APPLICATION_P_H
