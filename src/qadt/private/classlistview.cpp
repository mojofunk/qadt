#include "qadt-private.h"

namespace qadt
{

ClassListView::ClassListView()
    : m_table(new ClassTableWidget(this))
{
    auto vlayout = new QVBoxLayout;

    vlayout->addWidget(m_table);

    setLayout(vlayout);
}

} // namespace qadt
