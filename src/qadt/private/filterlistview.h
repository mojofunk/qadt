#ifndef QADT_FILTERLISTVIEW_H
#define QADT_FILTERLISTVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class FilterTableWidget;

class FilterListView : public QWidget
{
    Q_OBJECT

public:
    FilterListView();

private:
    FilterTableWidget* m_table;
};

} // namespace qadt

#endif // QADT_FILTERLISTVIEW_H
