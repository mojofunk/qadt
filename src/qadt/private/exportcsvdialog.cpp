#include "qadt-private.h"

namespace qadt
{

ExportCSVDialog::ExportCSVDialog(const std::shared_ptr<adt::Record>& record,
                                 bool using_selected_record)
    : QDialog(ApplicationPrivate::instance()->developerWindow())
    , m_record(record)
    , m_options(new adt::CSVOptions(record))
    , m_addTimestamp(new QCheckBox("Add Timestamp as first column"))
    , m_addDuration(new QCheckBox("Add Duration as second column"))
{
    setWindowTitle("Export to CSV");

    auto vLayout = new QVBoxLayout;
    vLayout->setSpacing(10);

    QString label_message = "Export to CSV file based on selected record";

    if (!using_selected_record) {
        label_message = "Export to CSV file based on first record in list";
    }

    vLayout->addWidget(new QLabel(label_message));

    auto gridWidget = new QWidget;
    auto gridLayout = new QGridLayout;

    gridLayout->addWidget(m_addTimestamp, 0, 0);
    gridLayout->addWidget(m_addDuration, 1, 0);
    gridWidget->setLayout(gridLayout);

    vLayout->addWidget(gridWidget);

    vLayout->addWidget(new QLabel("Include properties in CSV:"));

    gridWidget = new QWidget;
    gridLayout = new QGridLayout;

    int row = 0;

    for (const auto& property : record->properties) {
        auto checkbox = new QCheckBox(QString::fromStdString(property.name));
        checkbox->setChecked(true);
        m_propertyCheckboxes.push_back(checkbox);
        gridLayout->addWidget(checkbox, row++, 0);
    }
    gridWidget->setLayout(gridLayout);
    vLayout->addWidget(gridWidget);

    auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    vLayout->addWidget(buttons);
    setLayout(vLayout);

    connect(buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

adt::CSVOptions const&
ExportCSVDialog::options() const
{
    m_options->add_timestamp = m_addTimestamp->isChecked();
    m_options->add_duration = m_addDuration->isChecked();

    for (std::size_t i = 0; i < m_record->properties.size(); ++i) {
        if(m_propertyCheckboxes[i]->isChecked()) {
           m_options->properties_enabled[i] = true;
        }
    }

	return *(m_options.get());
}

} // namespace qadt
