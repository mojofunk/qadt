#ifndef QADT_FILTERDIALOG_H
#define QADT_FILTERDIALOG_H

#include "qadt/headerincludes.h"

namespace qadt
{

class FilterDialog : public QDialog
{
    Q_OBJECT

public: // Ctors
    /**
     * Create FilterDialog from the ThreadListView
     */
    FilterDialog(const std::shared_ptr<adt::ThreadInfo>& thread_info);

    /**
     * Create FilterDialog from the LogRecordListView
     */
    FilterDialog(const std::shared_ptr<adt::Record>& record);

    /**
     * Create FilterDialog for editing a filter from the FilterListView.
     */
    FilterDialog(const std::shared_ptr<adt::Filter>& p_filter);

public: // methods
    std::shared_ptr<adt::Filter> filter() { return m_filter; }

private:
    void init();

    void createWidgets();

    void packGrid();

    void updateWidgetSensitivity();

    void setWidgetsFromFilter();

    void updateFilterFromWidgets();

    void connectSignals();

private Q_SLOTS:

    void onCheckBoxToggled(bool);

    void onComboBoxTextChanged(const QString&);

public: // Data
    std::shared_ptr<adt::Filter> m_filter{ new adt::Filter() };

    QWidget* m_gridWidget;

    QDialogButtonBox* m_buttons;

    QCheckBox* m_notMatchingCheckBox;

    QCheckBox* m_threadIDCheckBox;
    QLabel* m_threadIDLabel;

    QCheckBox* m_threadPriorityCheckBox;
    QComboBox* m_threadPriorityComboBox;

    QCheckBox* m_instanceCheckBox;
    QLabel* m_instanceLabel;

    QCheckBox* m_filePathCheckBox;
    QLabel* m_filePathLabel;

    QCheckBox* m_functionCheckBox;
    QLabel* m_functionLabel;
};

} // namespace qadt

#endif // QADT_FILTERDIALOG_H
