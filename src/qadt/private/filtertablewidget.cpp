#include "qadt-private.h"

namespace qadt
{

FilterTableWidget::FilterTableWidget(QWidget* parent)
    : QTableWidget(parent)
{
    setColumnCount(Columns::COUNT);
    QStringList columns;
    columns << "Matching"
            << "Thread ID"
            << "Thread Priority"
            << "Instance"
            << "File Path"
            << "Function Name";
    setHorizontalHeaderLabels(columns);

    setEditTriggers(QAbstractItemView::NoEditTriggers);

    verticalHeader()->hide();
    setShowGrid(false);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &QWidget::customContextMenuRequested, this,
            &FilterTableWidget::onCustomMenuRequested);

    connect(this, &QTableView::doubleClicked, this,
            &FilterTableWidget::onDoubleClick);

    connect(ApplicationPrivate::instance(),
            &ApplicationPrivate::signalFiltersChanged, this,
            &FilterTableWidget::refresh);
}

void
FilterTableWidget::onCustomMenuRequested(const QPoint& pos)
{
    auto const& index = indexAt(pos);

    if (!index.isValid()) {
        return;
    }

    QMenu* menu = new QMenu(this);

    QAction* editAction = new QAction(tr("&Edit..."), this);
    editAction->setStatusTip("Edit the filter");
    connect(editAction, &QAction::triggered, [=]() { editFilter(index); });
    menu->addAction(editAction);

    QAction* delAction = new QAction(tr("&Delete..."), this);
    delAction->setShortcuts(QKeySequence::Delete);
    delAction->setStatusTip("Delete the filter");
    connect(delAction, &QAction::triggered, [=]() { deleteFilter(index); });
    menu->addAction(delAction);

    menu->popup(viewport()->mapToGlobal(pos));
}

void
FilterTableWidget::keyPressEvent(QKeyEvent* event)
{
    if (event->key() != Qt::Key_Delete) {
        return;
    }

     deleteFilter(selectionModel()->currentIndex());
}

void
FilterTableWidget::onDoubleClick(const QModelIndex& index)
{
    editFilter(index);
}

void
FilterTableWidget::editFilter(const QModelIndex& index)
{
    if (!index.isValid()) {
        return;
    }

    QVariant variant = item(index.row(), Columns::MATCHING)->data(Qt::UserRole);
    auto filter = qvariant_cast<std::shared_ptr<adt::Filter>>(variant);

    FilterDialog dialog(filter);

    if (dialog.exec() == QDialog::Accepted) {
        ApplicationPrivate::instance()->removeFilter(filter);
        ApplicationPrivate::instance()->addFilter(dialog.filter());
    }
}

void
FilterTableWidget::deleteFilter(const QModelIndex& index)
{
    if (!index.isValid()) {
        return;
    }

    QVariant variant = item(index.row(), Columns::MATCHING)->data(Qt::UserRole);
    auto filter = qvariant_cast<std::shared_ptr<adt::Filter>>(variant);
    ApplicationPrivate::instance()->removeFilter(filter);
}

void
FilterTableWidget::refresh()
{
    auto const filters = adt::Log::get_filters();

    std::set<int> rowIndexes;

    for (int row = 0; row < rowCount(); ++row) {
        rowIndexes.insert(row);
    }

    // Add all filters not already represented
    for (auto const& filter : filters) {
        bool found = false;

        auto const sMatching = filter->not_matching ? QString("Not Matching")
                                                    : QString("Matching");

        auto const sThreadID = filter->match_thread_id
            ? threadIdToString(filter->thread_id)
            : QString();

        auto const sThreadPriority = filter->match_thread_priority
            ? threadPriorityToString(filter->thread_priority)
            : QString();

        auto const sInstance = filter->match_instance
            ? instanceToString(filter->instance_ptr)
            : QString();

        auto const sFilePath = filter->match_file_path
            ? QString::fromStdString(filter->file_path)
            : QString();

        auto const sFunctionName = filter->match_function
            ? QString::fromStdString(filter->function_name)
            : QString();

        for (auto it = rowIndexes.begin(); it != rowIndexes.end(); ++it) {
            QVariant variant = item(*it, Columns::MATCHING)->data(Qt::UserRole);
            auto f = qvariant_cast<std::shared_ptr<adt::Filter>>(variant);

            if (f == filter) {
                found = true;
                item(*it, Columns::MATCHING)->setText(sMatching);
                item(*it, Columns::THREAD_NAME)->setText(sThreadID);
                item(*it, Columns::THREAD_PRIORITY)->setText(sThreadPriority);
                item(*it, Columns::INSTANCE)->setText(sInstance);
                item(*it, Columns::FILE_PATH)->setText(sFilePath);
                item(*it, Columns::FUNCTION_NAME)->setText(sFunctionName);
                rowIndexes.erase(it);
                break;
            }
        }

        if (!found) {
            auto const newRow = rowCount();
            insertRow(newRow);
            auto item = new QTableWidgetItem(sMatching);
            QVariant variant;
            variant.setValue(filter);
            item->setData(Qt::UserRole, variant);
            setItem(newRow, Columns::MATCHING, item);

            setItem(newRow, Columns::THREAD_NAME,
                    new QTableWidgetItem(sThreadID));
            setItem(newRow, Columns::THREAD_PRIORITY,
                    new QTableWidgetItem(sThreadPriority));
            setItem(newRow, Columns::INSTANCE, new QTableWidgetItem(sInstance));
            setItem(newRow, Columns::FILE_PATH,
                    new QTableWidgetItem(sFilePath));
            setItem(newRow, Columns::FUNCTION_NAME,
                    new QTableWidgetItem(sFunctionName));
        }
    }

    // Remove all filters that no longer exist from the bottom so index
    // isn't invalidated
    for (auto it = rowIndexes.rbegin(); it != rowIndexes.rend(); ++it) {
        removeRow(*it);
    }
}

} // namespace qadt
