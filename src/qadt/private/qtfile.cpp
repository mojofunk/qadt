#include "qadt-private.h"

QtFile::QtFile(QString const& file_path)
    : m_file(file_path)
{
}

QtFile::~QtFile()
{
    close();
}

bool
QtFile::open()
{
    m_file.open(QFile::WriteOnly | QFile::Truncate);

    return m_file.isOpen();
}

bool
QtFile::close()
{
    m_file.close();
    return true;
}

bool
QtFile::write(std::string const& str)
{
    return (m_file.write(str.c_str(), str.size()) != -1);
}
