#include "qadt-private.h"

namespace qadt
{

LogCategoryControl::LogCategoryControl()
{
    auto vlayout = new QVBoxLayout();

    auto searchLineEdit = new SearchLineEdit();
    auto listWidget = new LogCategoryListWidget(this);

    searchLineEdit->setClearButtonEnabled(true);
    // searchLineEdit->addAction(QIcon(":/resources/search.ico"),
    // QLineEdit::LeadingPosition);
    searchLineEdit->setPlaceholderText("Search Categories...");

    vlayout->addWidget(searchLineEdit);
    vlayout->addWidget(listWidget);

    setLayout(vlayout);

    connect(searchLineEdit, &QLineEdit::textChanged, listWidget,
            &LogCategoryListWidget::setCategoryFilter);

    connect(searchLineEdit, &QLineEdit::returnPressed, listWidget,
            &LogCategoryListWidget::toggleVisibleItemsEnabled);
}

QSize
LogCategoryControl::sizeHint() const
{
    return QSize(100, 400);
}

} // namespace qadt
