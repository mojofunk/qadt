#ifndef DEVELOPERWINDOW_P_H
#define DEVELOPERWINDOW_P_H

#include "qadt/headerincludes.h"

namespace qadt
{

class DeveloperWindowPrivate : public QWidget
{
    Q_OBJECT

public:
    DeveloperWindowPrivate();
};

} // namespace qadt

#endif // DEVELOPERWINDOW_P_H
