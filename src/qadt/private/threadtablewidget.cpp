#include "qadt-private.h"

namespace qadt
{

ThreadTableWidget::ThreadTableWidget(QWidget* parent)
    : QTableWidget(parent)
{
    setColumnCount(Columns::COUNT);
    QStringList columns;
    columns << "Thread ID"
            << "Thread Name"
            << "Priority";
    setHorizontalHeaderLabels(columns);
    setSortingEnabled(true);

    setEditTriggers(QAbstractItemView::NoEditTriggers);

    verticalHeader()->hide();
    setShowGrid(false);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &QWidget::customContextMenuRequested, this,
            &ThreadTableWidget::onCustomMenuRequested);

    connect(this, &QTableView::doubleClicked, this,
            &ThreadTableWidget::onDoubleClick);

    connect(&ApplicationPrivate::instance()->autoUpdateTimer(),
            &QTimer::timeout, this, &ThreadTableWidget::refresh);
}

void
ThreadTableWidget::refresh()
{
    std::vector<std::shared_ptr<adt::ThreadInfo>> allThreadInfo;

    if (!adt::Threads::get_all_thread_info(allThreadInfo)) {
        return;
    }

    std::set<int> rowIndexes;

    for (int row = 0; row < rowCount(); ++row) {
        rowIndexes.insert(row);
    }

    setSortingEnabled(false);

    // Add all threads not already represented
    for (auto const& threadInfo : allThreadInfo) {
        bool found = false;

        auto const sID = threadIdToString(threadInfo->id);
        auto const sName = QString::fromStdString(threadInfo->name);

        auto const sPriority = threadPriorityToString(threadInfo->priority);

        for (auto it = rowIndexes.begin(); it != rowIndexes.end(); ++it) {

            QVariant vInfo = item(*it, Columns::ID)->data(Qt::UserRole);
            auto info = qvariant_cast<std::shared_ptr<adt::ThreadInfo>>(vInfo);

            if (info == threadInfo) {
                rowIndexes.erase(it);
                found = true;
                break;
            }
        }

        if (!found) {
            auto const newRow = rowCount();
            insertRow(newRow);
            auto item = new QTableWidgetItem(sID);
            QVariant vInfo;
            vInfo.setValue(threadInfo);
            item->setData(Qt::UserRole, vInfo);
            setItem(newRow, Columns::ID, item);
            setItem(newRow, Columns::NAME, new QTableWidgetItem(sName));
            setItem(newRow, Columns::PRIORITY, new QTableWidgetItem(sPriority));
        }
    }

    // Remove all threads that no longer exist from the bottom so index
    // isn't invalidated
    for (auto it = rowIndexes.rbegin(); it != rowIndexes.rend(); ++it) {
        removeRow(*it);
    }

    setSortingEnabled(true);
}

void
ThreadTableWidget::onCustomMenuRequested(const QPoint& pos)
{
    auto const& index = indexAt(pos);

    if (!index.isValid()) {
        return;
    }

    QMenu* menu = new QMenu(this);

    QAction* addAction = new QAction(tr("&Add Thread Filter..."), this);
    addAction->setStatusTip("Filter thread");
    connect(addAction, &QAction::triggered, [=]() { addFilter(index); });
    menu->addAction(addAction);

    menu->popup(viewport()->mapToGlobal(pos));
}

void
ThreadTableWidget::onDoubleClick(const QModelIndex& index)
{
    addFilter(index);
}

void
ThreadTableWidget::addFilter(const QModelIndex& index)
{
    if (!index.isValid()) {
        return;
    }

    QVariant vInfo = item(index.row(), Columns::ID)->data(Qt::UserRole);
    auto info = qvariant_cast<std::shared_ptr<adt::ThreadInfo>>(vInfo);

    FilterDialog dialog(info);

    if (dialog.exec() == QDialog::Accepted) {
        ApplicationPrivate::instance()->addFilter(dialog.filter());
    }
}

} // namespace qadt
