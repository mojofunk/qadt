#ifndef QADT_QTTYPES_H
#define QADT_QTTYPES_H

Q_DECLARE_METATYPE(std::shared_ptr<adt::Record>);
Q_DECLARE_METATYPE(std::shared_ptr<adt::ThreadInfo>);
Q_DECLARE_METATYPE(adt::ClassTracker*);
Q_DECLARE_METATYPE(std::shared_ptr<adt::Filter>);
Q_DECLARE_METATYPE(adt::LogCategory*);
Q_DECLARE_METATYPE(std::shared_ptr<adt::StackFrame>);

#endif // QADT_QTTYPES_H
