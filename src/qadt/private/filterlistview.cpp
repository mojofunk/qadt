#include "qadt-private.h"

namespace qadt
{

FilterListView::FilterListView()
    : m_table(new FilterTableWidget(this))
{
    auto vlayout = new QVBoxLayout;

    vlayout->addWidget(m_table);

    setLayout(vlayout);
}

} // namespace qadt
