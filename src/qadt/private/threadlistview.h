#ifndef QADT_THREADLISTVIEW_H
#define QADT_THREADLISTVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class ThreadTableWidget;

class ThreadListView : public QWidget
{
    Q_OBJECT

public:
    ThreadListView();

private:
    ThreadTableWidget* m_table;
};

} // namespace qadt

#endif // QADT_THREADLISTVIEW_H
