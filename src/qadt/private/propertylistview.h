#ifndef QADT_PROPERTYLISTVIEW_H
#define QADT_PROPERTYLISTVIEW_H

#include "qadt/headerincludes.h"

namespace qadt
{

class PropertyListView : public QWidget
{
    Q_OBJECT

public:
    PropertyListView();

public Q_SLOTS:
    void setRecord(std::shared_ptr<adt::Record>);

private:
    enum Columns { NAME, VALUE, TYPE, FUNCTION, COUNT };

private:
    QTableWidget* m_tableWidget;
    std::shared_ptr<adt::Record> m_record;
};

} // namespace qadt

#endif // QADT_PROPERTYLISTVIEW_H
