#ifndef QADT_LOGCATEGORYLISTWIDGET_H
#define QADT_LOGCATEGORYLISTWIDGET_H

#include "qadt/headerincludes.h"

namespace qadt
{

class LogCategoryListWidget : public QListWidget
{
    Q_OBJECT

public:
    LogCategoryListWidget(QWidget* parent);

public Q_SLOTS:
    void setCategoryFilter(const QString& filter);

    void onItemChanged(QListWidgetItem*);

    void toggleVisibleItemsEnabled();

private:
    void refresh();

private:
    QStringList m_categoryStringList;
};

} // namespace qadt

#endif // QADT_LOGCATEGORYLISTWIDGET_H
