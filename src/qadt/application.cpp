#include "qadt-private.h"

namespace qadt
{

Application::Application()
{
    ApplicationPrivate::instance()->setLogConfigFromLibraryOptions();
}

Application::Application(int* argc, char*** argv)
{
    ApplicationPrivate::instance()->parseCommandLine(argc, argv);
}

Application::~Application()
{
    delete ApplicationPrivate::instance();
}

DeveloperWindow*
Application::developerWindow()
{
    return ApplicationPrivate::instance()->developerWindow();
}

void
Application::setSourceSearchPaths(std::vector<std::string> const& paths)
{
    ApplicationPrivate::instance()->setSourceSearchPaths(paths);
}

std::vector<std::string>
Application::sourceSearchPaths()
{
    return ApplicationPrivate::instance()->sourceSearchPaths();
}

} // namespace qadt
