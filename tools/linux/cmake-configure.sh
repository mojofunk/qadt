#! /bin/bash

. cmake-env.sh

rm -rf $BUILD_DIR

mkdir $BUILD_DIR && cd $BUILD_DIR

cmake -GNinja $BUILD_TYPE "-DBUILD_SHARED_LIBS=1" "-DCMAKE_INSTALL_PREFIX:PATH=/usr" "$@" ../../..
