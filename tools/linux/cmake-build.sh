#! /bin/bash

. cmake-env.sh

cd $BUILD_DIR && ninja -v "$@"
