#! /bin/bash

if hash clang-format 2>/dev/null; then
    find ../src/qadt -type f -name "*.cpp" | xargs clang-format -i "$@"
    find ../src/qadt -type f -name "*.h" | xargs clang-format -i "$@"
    find ../examples -type f -name "*.cpp" | xargs clang-format -i "$@"
    find ../examples -type f -name "*.h" | xargs clang-format -i "$@"
else
    echo "clang-format is not installed, cannot automatically format source code"
fi
