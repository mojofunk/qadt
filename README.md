# qadt

qadt is a [Qt](https://www.qt.io/) based GUI for the
[adt](https://gitlab.com/mojofunk/adt) library and is used to control
the instrumentation and visualize the logging.

## Features

- Control log record types (message, data, function calls,
  class allocation, global allocation).
- Log viewer with search function.
- Source code viewer.
- Thread list.
- Filtering of log records by Thread ID, Priority, File, Function.
- Stack/Backtrace View.
- Class list for classes using the adt class instrumentation macros with
  size, count and copied count.

While developing and testing qadt I added some basic instrumentation to
the LMMS codebase.

<iframe src="https://player.vimeo.com/video/276233224" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/276233224">qadt and LMMS</a> from <a href="https://vimeo.com/user82314406">Tim Mayberry</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## Documentation

There is none currently.

To get a further idea of how to use the interface you
can watch a presentation I made in relation to using it with
[Ardour](https://ardour.org)

<iframe src="https://player.vimeo.com/video/260590555" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/260590555">Instrumenting Ardour</a> from <a href="https://vimeo.com/user82314406">Tim Mayberry</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## License

qadt is licensed under the GNU GPLv3.

## Building

The included CMake build script can be used to build and install the qadt
library.

Currently only builds on Linux (Fedora) with gcc and Windows with MSVC
2017 have been tested(via vcpkg).
