#include "qadt.h"

#include "hello_world_window.h"

#include <QApplication>

int
main(int argc, char** argv)
{
    QApplication app(argc, argv);

    qadt::Application qadt_app(&argc, &argv);

    A_REGISTER_THREAD("Example Main Thread", adt::ThreadPriority::NORMAL);

    qadt::Application::developerWindow()->show();

    HelloWorldWindow win;
    win.show();

    return app.exec();
}
