#ifndef HELLO_WORLD_WINDOW_H
#define HELLO_WORLD_WINDOW_H

#include "qadt.h"

class HelloWorldWindow : public QMainWindow
{
    Q_OBJECT;

public:
    HelloWorldWindow();

private:
    void task_thread();

    void start_task_threads();

private:
    A_DECLARE_CLASS_MEMBERS(HelloWorldWindow);
};

#endif // HELLO_WORLD_WINDOW_H
