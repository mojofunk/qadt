#include "hello_world_window.h"

#include <QtWidgets>

A_DEFINE_CLASS_MEMBERS(HelloWorldWindow)

class ATestClass
{

private:
    A_DECLARE_CLASS_MEMBERS(ATestClass)
};

A_DEFINE_CLASS_MEMBERS(ATestClass)

class ZTestClass
{

private:
    A_DECLARE_CLASS_MEMBERS(ZTestClass)
};

A_DEFINE_CLASS_MEMBERS(ZTestClass)

void
HelloWorldWindow::task_thread()
{
    A_REGISTER_THREAD("Task Thread", adt::ThreadPriority::NORMAL)

    A_CLASS_CALL()

    ATestClass atest;
    ZTestClass ztest;

    for (int i = 0; i < 10; ++i) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        A_CLASS_MSG(A_FMT("Progess {}/10", i + 1));

        float afloat = i;
        int32_t aint = i + 1;
        A_CLASS_DATA2(aint, afloat);

        afloat += 0.5f;

        A_CLASS_DATA1(afloat);
    }
}

void
HelloWorldWindow::start_task_threads()
{
    std::vector<std::thread> test_threads;

    const int num_test_threads = std::thread::hardware_concurrency() * 2;

    for (int i = 0; i != num_test_threads; ++i) {
        test_threads.push_back(
            std::thread(&HelloWorldWindow::task_thread, this));
    }

    for (auto& t : test_threads) {
        t.detach();
    }
}

HelloWorldWindow::HelloWorldWindow()
{
    auto vlayout = new QVBoxLayout;

    auto button = new QPushButton(this);

    button->setText("Hello World");

    connect(button, &QAbstractButton::clicked, this, [=] {
        A_CLASS_CALL();

        A_CLASS_MSG("Hello World");

        start_task_threads();

        std::this_thread::sleep_for(std::chrono::microseconds(200));

        A_CLASS_MSG("Goodbye World");

        float afloat = 1.342f;
        int32_t aint = 123456789;

        A_CLASS_DATA2(afloat, aint);
    });

    vlayout->addWidget(button);
    setLayout(vlayout);

    setCentralWidget(button);
}
