configure_file(
    ${CMAKE_MODULE_PATH}/qadt.pc.in
    ${CMAKE_CURRENT_BINARY_DIR}/${QADT_TARGET}.pc
    @ONLY
)

install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/${QADT_TARGET}.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
)
