message("------------------------------------------------------------------------")
message("")
message("Configuration Summary - qadt")
message("")

if(QADT_MASTER_PROJECT)
    include(FeatureSummary)
    feature_summary(WHAT ALL
                    DESCRIPTION "qadt Features:"
                    VAR allFeaturesText)
    message(STATUS "${allFeaturesText}")

    # cmake info
    message(STATUS "CMAKE_BINARY_DIR:        ${CMAKE_BINARY_DIR}")
    message(STATUS "CMAKE_INSTALL_PREFIX:    ${CMAKE_INSTALL_PREFIX}")
    message(STATUS "CMAKE_SYSTEM_NAME:       ${CMAKE_SYSTEM_NAME}")
    message(STATUS "CMAKE_SYSTEM_VERSION:    ${CMAKE_SYSTEM_VERSION}")
    message(STATUS "CMAKE_SYSTEM_PROCESSOR:  ${CMAKE_SYSTEM_PROCESSOR}")
    message(STATUS "CMAKE_C_COMPILER:        ${CMAKE_C_COMPILER}")
    message(STATUS "CMAKE_CXX_COMPILER:      ${CMAKE_CXX_COMPILER}")
    message(STATUS "CMAKE_BUILD_TYPE:        ${CMAKE_BUILD_TYPE}")
    message("")
endif()

# qadt specific
message(STATUS "QADT_VERSION:                 ${QADT_VERSION}")
message(STATUS "QADT_BUILD_SHARED_LIBS:       ${QADT_BUILD_SHARED_LIBS}")
message(STATUS "QADT_INSTALL_HEADERS:         ${QADT_INSTALL_HEADERS}")
message(STATUS "QADT_INSTALL_PKGCONFIG:       ${QADT_INSTALL_PKGCONFIG}")
message(STATUS "QADT_INSTALL_CMAKECONFIG:     ${QADT_INSTALL_CMAKECONFIG}")
message(STATUS "QADT_BUILD_EXAMPLES:          ${QADT_BUILD_EXAMPLES}")

message("")
message("------------------------------------------------------------------------")
