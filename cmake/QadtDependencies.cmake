if(NOT TARGET adt-0::adt-0)
    find_package(adt-0 REQUIRED)
endif()

find_package(Qt5 REQUIRED COMPONENTS Core Widgets Gui)
