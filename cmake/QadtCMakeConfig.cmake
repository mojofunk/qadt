set(QADT_INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/${QADT_TARGET})

install(EXPORT ${QADT_TARGET}-targets
    FILE ${QADT_TARGET}-targets.cmake
    NAMESPACE ${QADT_TARGET}::
    DESTINATION ${QADT_INSTALL_CONFIGDIR}
)

include(CMakePackageConfigHelpers)

configure_package_config_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/qadt-config.cmake.in
    ${CMAKE_BINARY_DIR}/cmake/${QADT_TARGET}-config.cmake
    INSTALL_DESTINATION ${QADT_INSTALL_CONFIGDIR}
)

# Use AnyNewerVersion here rather than SameMajorVersion as the namespace
# contains the major version. Unless I'm missing some detail about how
# this is all supposed to work.
write_basic_package_version_file(
    ${CMAKE_BINARY_DIR}/cmake/${QADT_TARGET}-config-version.cmake
    VERSION ${QADT_VERSION}
    COMPATIBILITY AnyNewerVersion
)

install(
    FILES
        ${CMAKE_BINARY_DIR}/cmake/${QADT_TARGET}-config.cmake
        ${CMAKE_BINARY_DIR}/cmake/${QADT_TARGET}-config-version.cmake
    DESTINATION ${QADT_INSTALL_CONFIGDIR}
)

export(EXPORT ${QADT_TARGET}-targets
    FILE ${CMAKE_BINARY_DIR}/cmake/${QADT_TARGET}-targets.cmake
    NAMESPACE ${QADT_TARGET}::
)
