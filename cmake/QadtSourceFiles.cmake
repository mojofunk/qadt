file(GLOB QADT_PUBLIC_HEADER "src/qadt.h")
file(GLOB QADT_PUBLIC_SUBHEADERS "src/qadt/*.h*")

file(GLOB QADT_PRIVATE_HEADER "src/qadt/qadt-private.h")
file(GLOB QADT_PRIVATE_SUBHEADERS "src/qadt/private/*.h")

file(GLOB QADT_PUBLIC_SOURCES "src/qadt/*.cpp")
file(GLOB QADT_PRIVATE_SOURCES "src/qadt/private/*.cpp")

file(GLOB QADT_UNITY_SOURCE "src/qadt.cpp")
file(GLOB QADT_UNITY_PRIVATE_SOURCE "src/qadt-private.cpp")

set(QADT_SOURCES
	"${QADT_PUBLIC_HEADER}"
	"${QADT_PUBLIC_SUBHEADERS}"
	"${QADT_PRIVATE_HEADER}"
	"${QADT_PRIVATE_SUBHEADERS}"
	"${QADT_UNITY_SOURCE}"
	"${QADT_UNITY_PRIVATE_SOURCE}"
)
