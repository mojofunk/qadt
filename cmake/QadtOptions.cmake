option(QADT_BUILD_SHARED_LIBS "Build shared library." OFF)
option(QADT_INSTALL_HEADERS "Install Header files." ${QADT_MASTER_PROJECT})
option(QADT_INSTALL_PKGCONFIG "Install pkg-config related files." ${QADT_MASTER_PROJECT})
option(QADT_INSTALL_CMAKECONFIG "Install CMake export target files." ${QADT_MASTER_PROJECT})
option(QADT_BUILD_EXAMPLES "Build example applications." ${QADT_MASTER_PROJECT})

if(MSVC OR MINGW)
	if(QADT_BUILD_SHARED_LIBS)
		# Disable shared library on Windows as replacement allocators only work with static
		message(FATAL_ERROR "Building a shared library on Windows is not supported")
	endif()
endif()
