set(HELLO_WORLD_SOURCES
    examples/main.cpp
    examples/hello_world_window.cpp
)

set(HELLO_WORLD_HEADERS_MOC
    examples/hello_world_window.h
)

qt5_wrap_cpp(HELLO_WORLD_SOURCES_MOC ${HELLO_WORLD_HEADERS_MOC})

set(HELLO_WORLD_EXAMPLE_SOURCES
    ${HELLO_WORLD_SOURCES}
    ${HELLO_WORLD_HEADERS_MOC}
    ${HELLO_WORLD_SOURCES_MOC}
)

find_package(Threads REQUIRED)

add_executable(qadt-hello-world-example ${HELLO_WORLD_EXAMPLE_SOURCES})

target_link_libraries(qadt-hello-world-example Threads::Threads ${QADT_TARGET}::${QADT_TARGET})
