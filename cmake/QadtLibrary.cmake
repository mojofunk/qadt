set(QADT_TARGET qadt-${QADT_VERSION_MAJOR})

set(CMAKE_AUTOMOC ON)

if(QADT_BUILD_SHARED_LIBS)
    add_library(${QADT_TARGET} SHARED ${QADT_SOURCES})
    target_compile_definitions(${QADT_TARGET} PRIVATE "QADT_BUILDING_DLL")
else()
    add_library(${QADT_TARGET} STATIC ${QADT_SOURCES})
    target_compile_definitions(${QADT_TARGET} PUBLIC "QADT_STATIC")
endif()

add_library(${QADT_TARGET}::${QADT_TARGET} ALIAS ${QADT_TARGET})

set_target_properties(${QADT_TARGET} PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    VERSION ${QADT_VERSION}
    SOVERSION ${QADT_VERSION_MAJOR}
)

#if(MSVC)
#    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4250")
#endif()

target_include_directories(${QADT_TARGET}
    PUBLIC
        $<INSTALL_INTERFACE:include/${QADT_TARGET}>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
    #PRIVATE
        # If needed for a private config.h
        #${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries(${QADT_TARGET}
    PUBLIC
    adt-0::adt-0
    Qt5::Core
    Qt5::Widgets
    Qt5::Gui
)
